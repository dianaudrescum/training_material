var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function processDataAsync(str) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(new Date() + "init: " + str);
        let res1 = yield task1async(str, 3000);
        console.log(new Date() + "res1: " + res1);
        let res2 = yield task2async(res1, 3000);
        console.log(new Date() + "res2: " + res2);
        let res3 = yield task3async(res2, 3000);
        console.log(new Date() + "res3: " + res3);
        return res3;
    });
}
function task1async(str, ms) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            setTimeout(() => resolve(str.trim()), ms);
        });
    });
}
function task2async(str, ms) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            setTimeout(() => resolve(str.toUpperCase()), ms);
        });
    });
}
function task3async(str, ms) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            setTimeout(() => resolve("<<<" + str + ">>>"), ms);
        });
    });
}
processDataAsync("   This is the day   ").then(res => {
    console.log(new Date() + "res:  " + res);
});
