var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function processData3(str) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(new Date() + "initial:   " + str);
        let results = yield Promise.all([
            task4async(str, 3000),
            task5async(str, 3000),
            task6async(str, 3000)
        ]);
        console.log(new Date() + "results:   " + results);
        return results;
    });
}
function task4async(str, ms) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            setTimeout(() => resolve(str.trim()), ms);
        });
    });
}
function task5async(str, ms) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            setTimeout(() => resolve(str.toUpperCase()), ms);
        });
    });
}
function task6async(str, ms) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise(resolve => {
            setTimeout(() => resolve("<<<" + str + ">>>"), ms);
        });
    });
}
processData3("   This is the day   ").then(endResult => {
    console.log(new Date() + "endResult: " + endResult);
});
