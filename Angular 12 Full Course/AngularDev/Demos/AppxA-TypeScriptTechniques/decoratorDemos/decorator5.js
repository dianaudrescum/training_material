var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// This is a class decorator that wraps the original ctor with additional semantic work.
function TimestampedInstanceParameterizedAndWrapped(date, time) {
    console.log("TimestampedInstanceParameterizedAndWrapped decorator factory called.");
    // This statement returns a function that creates a new ctor. The new ctor augments the original ctor.
    return function (ctor) {
        console.log("TimestampedInstanceParameterizedAndWrapped decorator called.");
        // Define a new ctor function.
        let newCtor = function (...args) {
            // Call the original ctor, to create a bare-bones instance.
            let obj = new ctor(...args);
            // Do some additional work on the instance.
            let ts = new Date();
            obj.tsDate = date ? ts.getDate() : "[n/a]";
            obj.tsTime = time ? ts.getSeconds() : "[n/a]";
            // Return the instance.
            return obj;
        };
        // The new ctor has the same prototype as the original ctor.
        newCtor.prototype = ctor.prototype;
        // Return the new ctor. This is what TypeScript will invoke, to instantiate decorated types.
        return newCtor;
    };
}
let Alarm = class Alarm {
    constructor(what) {
        console.log("Alarm constructor called.");
        this.what = what;
    }
};
Alarm = __decorate([
    TimestampedInstanceParameterizedAndWrapped(false, true)
], Alarm);
// Client code.
let a1 = new Alarm("Caffeine level low");
console.log(a1);
console.log(`a1 tsDate: ${a1['tsDate']}`);
console.log(`a1 tsTime: ${a1['tsTime']}`);
setTimeout(() => {
    let a2 = new Alarm("Caffeine level critical!");
    console.log(a2);
    console.log(`a2 tsDate: ${a2['tsDate']}`);
    console.log(`a2 tsTime: ${a2['tsTime']}`);
}, 5000);
