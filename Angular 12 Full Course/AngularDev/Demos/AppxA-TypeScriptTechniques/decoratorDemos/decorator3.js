var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// This is a class decorator that extends (i.e. subclasses) the original ctor/class, e.g. to add new properties to instances.
function TimestampedInstance(ctor) {
    console.log("TimestampedInstance decorator called.");
    // This syntax creates a class that extends the original constructor with a ts property.
    return class extends ctor {
        constructor() {
            super(...arguments);
            this.ts = new Date();
        }
    };
}
let Car = class Car {
    constructor(make, model) {
        console.log("Car constructor called.");
        this.make = make;
        this.model = model;
    }
};
Car = __decorate([
    TimestampedInstance
], Car);
// Client code.
let c1 = new Car("Bugatti", "Divo");
console.log(c1);
console.log(`c1 ts: ${c1['ts']}`);
setTimeout(() => {
    let c2 = new Car("Mazda", "6");
    console.log(c2);
    console.log(`c2 ts: ${c2['ts']}`);
}, 5000);
