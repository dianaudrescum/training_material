var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// This is a simple class decorator.
// A class decorator is applied to the constructor of the class and can be used to observe, modify, or replace a class definition.
// This example adds a property to the constructor's prototype object, so all instances of the decorated class will have this property.
function Secure(ctor) {
    console.log("Secure decorator called.");
    ctor.prototype.secure = true;
}
let CreditCard = class CreditCard {
    constructor(number, expiryDate, cvv) {
        console.log("CreditCard constructor called.");
        this.number = number;
        this.expiryDate = expiryDate;
        this.cvv = cvv;
    }
};
CreditCard = __decorate([
    Secure
], CreditCard);
class Person {
    constructor(name) {
        console.log("Person constructor called.");
        this.name = name;
    }
}
// Client code.
let cc1 = new CreditCard("1234567890123456", "0723", "123");
console.log(cc1);
console.log(`cc1 secure? ${cc1['secure']}`); // true
let p1 = new Person("Paul");
console.log(p1);
console.log(`p1 secure? ${p1['secure']}`); // undefined
