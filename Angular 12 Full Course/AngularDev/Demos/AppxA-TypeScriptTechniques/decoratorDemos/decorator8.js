var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
// This is parameter decorator. It makes use of a helper class named Logger, to keep track of all the @LogParam properties in all the methods in all the classes.
function LogParam(targetProto, methodName, paramIndex) {
    Logger.registerParam(targetProto, methodName, paramIndex);
}
// This is method decorator. It enhances a @LogParams-decorated method, to do the work of logging each of its @LogParam-decorated parameters.
function LogParams(targetProto, methodName, descriptor) {
    console.log("LogParams decorator called.");
    var originalMethod = descriptor.value;
    descriptor.value = function (...args) {
        // Use the Logger helper class to log the @LogParam-decorated parameters for this method.
        Logger.log(targetProto, methodName, args);
        let result = originalMethod.apply(this, args);
        return result;
    };
}
class Logger {
    // Register a particular @LogParam-decorated parameter for a particular method in a particular target type.
    static registerParam(targetProto, methodName, paramIndex) {
        // Get the mapr of methodNames-to-loggedParamIndices for a target prototype.
        let loggedParams = Logger.mapLoggedParams.get(targetProto);
        if (!loggedParams) {
            loggedParams = new Map();
            Logger.mapLoggedParams.set(targetProto, loggedParams);
        }
        // Get the loggedParamIndices for a method.
        let paramIndexes = loggedParams.get(methodName);
        if (!paramIndexes) {
            paramIndexes = [];
            loggedParams.set(methodName, paramIndexes);
        }
        // Add the specified parameter to the list of  loggedParamIndices for a method.
        paramIndexes.push(paramIndex);
    }
    // Do the work of logging all the @LogParam-decorated parameters for a target object, for a particular method.
    static log(target, methodName, paramValues) {
        let loggedParams = Logger.mapLoggedParams.get(target);
        if (!loggedParams) {
            return;
        }
        let paramIndexes = loggedParams.get(methodName);
        if (!paramIndexes) {
            return;
        }
        for (const [index, paramValue] of paramValues.entries()) {
            if (paramIndexes.indexOf(index) != -1) {
                console.log(`Method ${methodName}, parameter at index ${index} has value ${paramValue}`);
            }
        }
    }
}
// This map contains a list of all the @LogParam-decorated parameters for methods in classes.
// The key is a class type (i.e. prototype).
// The value is another map:
//	 The key is a method name.
//   The value is an array of indices, for parameters that are decorated with @LogParam.
Logger.mapLoggedParams = new Map();
class SomeClass {
    someMethod(param1, param2, param3) {
        console.log(`${param1} ${param2} ${param3}`);
    }
}
__decorate([
    LogParams,
    __param(1, LogParam), __param(2, LogParam)
], SomeClass.prototype, "someMethod", null);
// Client code.
let sc1 = new SomeClass();
sc1.someMethod("Hello", 42, "Wibble");
