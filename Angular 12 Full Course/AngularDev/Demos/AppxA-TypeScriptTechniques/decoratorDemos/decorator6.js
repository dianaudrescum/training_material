var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// This is a parameterized method decorator. The function is a factory that receives the decorator args, and returns a function to enhance the targeted method.
function Log(prefix, suffix) {
    console.log("Log decorator factory called.");
    // TypeScript will call this function whenever you apply the Log decorator to a method. The arguments here tell you about the decorated method.
    return function (target, propertyKey, descriptor) {
        console.log("Log decorator called.");
        // This is the original method body.
        var originalMethod = descriptor.value;
        // Change the method body to do some additional cool thangs.
        descriptor.value = function (...args) {
            // Here's some pre-logging baby.
            console.log(`${prefix} Before invoking ${propertyKey.toString()} with args ${args} ${suffix}`);
            // Here we call the original method. WE DON'T HAVE TO DO THIS if we don't want to.
            let result = originalMethod.apply(this, args);
            // Here's some post-logging man.
            console.log(`${prefix} After  invoking ${propertyKey.toString()}, returned ${result} ${suffix}`);
            return result;
        };
    };
}
class Greeter {
    hello(name) {
        return "hei hei " + name;
    }
    bye(name) {
        return "bye bye " + name;
    }
}
__decorate([
    Log("<<<", ">>>")
], Greeter.prototype, "hello", null);
__decorate([
    Log("[[[", "]]]")
], Greeter.prototype, "bye", null);
// Client code.
let g1 = new Greeter();
console.log(g1.hello("Messi"));
console.log(g1.hello("Bale"));
console.log(g1.bye("Gylfi"));
console.log(g1.bye("Wilf"));
