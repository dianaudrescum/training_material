var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// This is property decorator. It makes use of a helper class named Validator, to keep track of all the @NotNull properties in all the classes.
function NotNull(targetProto, propertyKey) {
    Validator.registerNotNull(targetProto, propertyKey);
}
class Validator {
    // Add a property to the list of @NotNull properties for a class type.
    static registerNotNull(targetProto, property) {
        let nonNullables = Validator.mapNonNullables.get(targetProto);
        if (!nonNullables) {
            nonNullables = [];
            this.mapNonNullables.set(targetProto, nonNullables);
        }
        nonNullables.push(property);
    }
    // A separate method that the client can call, to perform a validation check on the properties for a target object.
    static validate(target) {
        // Get the prototype for the target object, from our map of nullable info.
        let nonNullProps = Validator.mapNonNullables.get(Object.getPrototypeOf(target));
        if (!nonNullProps) {
            return true;
        }
        // Check all the @NotNull properties for the target object, to see if they are null (boo) or not (hooray).
        let hasErrors = false;
        for (const property of nonNullProps) {
            let value = target[property];
            if (!value) {
                console.error(property + " value cannot be null");
                hasErrors = true;
            }
        }
        return !hasErrors;
    }
}
// This map contains a list of all the @NotNull properties per class type.
// The key is a class type (i.e. prototype).
// The value is an array of @NotNull properties in that class type.
Validator.mapNonNullables = new Map();
class DbConn {
    constructor(server, db, username = null, password = null) {
        this.server = server;
        this.db = db;
        this.username = username;
        this.password = password;
    }
}
__decorate([
    NotNull
], DbConn.prototype, "server", void 0);
__decorate([
    NotNull
], DbConn.prototype, "db", void 0);
// Client code.
let db1 = new DbConn("server1", "db1");
console.log(db1);
console.log(`db1 valid? ${Validator.validate(db1)}`);
let db2 = new DbConn("server2", null);
console.log(db2);
console.log(`db2 valid? ${Validator.validate(db2)}`);
