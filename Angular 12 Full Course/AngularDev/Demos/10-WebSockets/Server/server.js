var WebSocketServer = require('websocket').server;
var http = require('http');
var fs = require('fs');


/* ================================================================================================= 
 * HTTP handler
 * =================================================================================================*/

// Create an HTTP server object.
var server = http.createServer(function(req, res) {

res.setHeader('Access-Control-Allow-Origin', '*');
    console.log((new Date()) + ' Received HTTP request for ' + req.url);

    // Allow CORS (cross-origin requests) from any client. In production, you'd want to do tighter checks here.
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', req.header.origin);
    if ( req.method === 'OPTIONS' ) {
        res.writeHead(200);
        res.end();
        return;
    }
});

// Start the HTTP server object listening on port 8080.
server.listen(8080, function() {
    console.log((new Date()) + ' Server is listening on port 8080');
});


/* ================================================================================================= 
 * Web Sockets server
 * =================================================================================================*/

// Create a WebSocketServer object.
var wsServer = new WebSocketServer({
    httpServer : server,              // Use the HTTP server object.
    autoAcceptConnections : false     // Don't automatically accept connections - always check origin!
});

// Maintain a collection of clients.
var clientList = new Array();

// Handle Web Socket requests.
wsServer.on('request', onWsRequest);

function onWsRequest(request) {

    console.log((new Date()) + 'Incoming WS request from ' + request.origin);
    var connection = request.accept(null, request.origin);
    clientList.push(connection);

    // Handle connection message events.
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log('Received text message: ' + message.utf8Data);
            sendToAllClients(message.utf8Data.toUpperCase());
        } 
        else if(message.type === 'binary') {
            console.log('Received binary message of ' + message.binaryData.length + ' bytes');
            connection.sendBytes(message.binaryData);
        } 
        else {
            console.log('Received unknown data-type ' + message.type);
        }
    });

    // Handle connection close events.
    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');

        // Remove connection from list.
        var idx = clientList.indexOf(connection);
        clientList.splice(idx, 1);
    });
}

// Helper function, sends a message to all current clients.
function sendToAllClients(text) {
    for(var i = clientList.length - 1; i >= 0; i--) {
        var client = clientList[i];
        if(client.connected) {
            console.log(new Date() + ' sending server message to ' + client.remoteAddress)
            client.sendUTF(text + ' [Connected clients=' + clientList.length + ']');
        }
    };
}
